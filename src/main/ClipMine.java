package main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.helper.Tag;
import com.helper.Video;


class ClipMine {
	
	final static String PROJECT_ID = "53e99654709a3930da000002";
	final static String WRITE_KEY = "5a19a17052df5018ab94754621420831c9e7811" +
			"86acbb113d4816c4bc77b0b506a1e452cd61e2589bd3f4c54e58d7aeec" +
			"a511e945d0721d7670a1f7d995b56a8b908214eaa230ea843df89c90bc" +
			"82a521e31c4010491c4568dd959c36fc3f69973546535851fd48821ee48d016cce2e1";
	final static String READ_KEY = "68d212f6c56a60904d1aca222cdfa5f6922604ae9b2d49dd5574d0e82738cd67441200583fc4f8f89c4838849211832b6c6005ea4e7f1d2eba1624d160e58e2a19f42d3da6456489f34da7e15ac0704189f11f8cd856ed4423e8611ce46601461588dec76e19583ccb205d398bfd9884";
	
	final static String MASTER_KEY = "41BC0F65CB6DE97570DA632AB9C39F5F";
	
	final static String video_id = "yt-JTZNZOf17N4";
	
	public static void main(String[] args) throws ClientProtocolException, IOException, JSONException{
		HashMap<String, Tag> map = new HashMap<String, Tag>();
		
		URL url1 = new URL("https://api.keen.io/3.0/projects/53e99654709a3930da000002/queries/extraction?");
		URL url2 = new URL("https://clip.mn/1/tags/?video_id="+video_id+"&format=json");
		
		
		HttpClient httpClient = new DefaultHttpClient();
		String json = analysis(url1, httpClient, "");
		
		org.json.JSONArray keen = readJSON(json);
		
		List<Tag> tags = detailsKeen(keen, map);
		System.out.println(tags.size());
		
		httpClient = new DefaultHttpClient();
		String jsonCm = analysisCM(url2, httpClient, "");
		org.json.JSONArray cm = readJSON(json);
		
		merege(cm, tags, map);
		
		Collections.sort(tags, comparator);
		System.out.println(tags.size());
		
		System.out.println("/n/nMost popular clicked tag ID for current video " + tags.get(0).id );
	}
	
	static Comparator<Tag> comparator = new Comparator<Tag>(){

		@Override
		public int compare(Tag o1, Tag o2) {
			// TODO Auto-generated method stub
			return o1.count - o2.count;
		}
		
	};
	
	private static List<Tag> merege(org.json.JSONArray json, List<Tag> list, HashMap<String, Tag> map) throws JSONException{
		List<Tag> tags = new ArrayList<Tag>();
		
		for (int i = 0; i < json.length(); i++){
			org.json.JSONObject cur = (JSONObject) json.get(i);
			String id = (String) cur.getString("tag_id");
			int playtime = (int) cur.getInt("start_time");
			
			map.get(id).length = playtime;
			
		}
		
		return tags;
	}
	
	private static List<Tag> detailsKeen(org.json.JSONArray keen, HashMap<String, Tag> map) throws JSONException{
		List<Tag> tags = new ArrayList<Tag>();
		
		for (int i = 0; i < keen.length(); i++){
			org.json.JSONObject cur = (JSONObject) keen.get(i);
			String id = (String) cur.getString("tag_id");
			int playtime = (int) cur.getInt("start_time");
			Tag t = new Tag(id, playtime);
			if (map.containsKey(id)){
				Tag tem = map.get(id);
				tem.count++;
			} else {
				map.put(id, t);
				tags.add(t);
			}
		}
		
		return tags;
	}
	
	private static org.json.JSONArray readJSON(String json) throws JSONException{
		List<Video> list = new ArrayList<Video>();
		
		org.json.JSONArray newArray = null;  
        JSONObject newJson = new JSONObject();  
        
        JSONObject obj = new JSONObject(json);
        newArray = obj.getJSONArray("result");
        return newArray;
		
	}
	
	private static org.json.JSONArray readJSONCM(String json) throws JSONException{
		org.json.JSONArray newArray = new org.json.JSONArray(json);  
        return newArray;
		
	}
	
	private static String analysis(URL url1, HttpClient httpClient, String videoId) throws ClientProtocolException, IOException{
		String url = url1.toString();
		url += "api_key=" + READ_KEY + "&event_collection=tag_play_time&video_id" + video_id + "&timeframe=previous_50_days";
		
		HttpGet getRequest = new HttpGet(url);
		
		HttpResponse response = httpClient.execute(getRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		} else {
			System.out.println("Success get data " + response.getStatusLine().getStatusCode());
		}
		
		String json = EntityUtils.toString(response.getEntity());
		
		
		httpClient.getConnectionManager().shutdown();
		
		return json;
	}
	
	private static String analysisCM(URL url1, HttpClient httpClient, String videoId) throws ClientProtocolException, IOException{
		String url = url1.toString();
		
		HttpGet getRequest = new HttpGet(url);
		
		HttpResponse response = httpClient.execute(getRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		} else {
			System.out.println("Success get data " + response.getStatusLine().getStatusCode());
		}
		
		String json = EntityUtils.toString(response.getEntity());
		
		
		httpClient.getConnectionManager().shutdown();
		
		return json;
	}
	
}
